baseurl = "https://detlefburkhardt.gitlab.io/graphugovizart/"
contentdir    = "content"
layoutdir     = "layouts"
publishdir    = "public"
title = "graphviz.org"
canonifyurls  = true

DefaultContentLanguage = "en"
theme = "springstyle"
metaDataFormat = "yaml"
pygmentsUseClasses = true
pygmentCodeFences = true
#disqusShortname = "XXX"
#googleAnalytics = "XXX"

[Params]
  subtitle = "Some investigations for a funtastic great facelift & refactoring"
  logo = "img/gv-logo.png"
  favicon = "img/favicon.ico"
  dateFormat = "January 2, 2018"
  commit = false
  rss = true
  comments = true
#  gcse = "012345678901234567890:abcdefghijk" # Get your code from google.com/cse. Make sure to go to "Look and Feel" and change Layout to "Full Width" and Theme to "Classic"

[[Params.bigimg]]
  src = "img/triangle.jpg"
  desc = "Triangle"
[[Params.bigimg]]
  src = "img/sphere.jpg"
  desc = "Sphere"
[[Params.bigimg]]
  src = "img/hexagon.jpg"
  desc = "Hexagon"

[Author]
  name = "MagicNorth, netDet vs. CodeFreezr & Friends"
  email = "youremail@domain.com"
  facebook = "username"
  googleplus = "+username" # or xxxxxxxxxxxxxxxxxxxxx
  gitlab = "username"
  github = "username"
  twitter = "username"
  reddit = "username"
  linkedin = "username"
  xing = "username"
  stackoverflow = "users/XXXXXXX/username"
  snapchat = "username"
  instagram = "username"
  youtube = "user/username" # or channel/channelname
  soundcloud = "username"
  spotify = "username"
  bandcamp = "username"
  itchio = "username"


[[menu.main]]
    name = "Start"
    url = "page/10_download/"
    weight = 1

[[menu.main]]
    identifier = "docs"
    name = "Documentation"
    weight = 2

[[menu.main]]
    parent = "docs"
    name = "Gallery"
    url = "page/02_gallery/"
    weight = 1

[[menu.main]]
    parent = "docs"
    name = "GraphViz Documentation"
    url = "page/docs/"
    weight = 1
    

[[menu.main]]
    parent = "docs"
    name = "Theory & Publications"
    url = "page/06_theory/"
    weight = 1
    
[[menu.main]]
    parent = "docs"
    name = "Resources"
    url = "page/04_resources/"
    weight = 1

[[menu.main]]
    parent = "docs"
    name = "FAQ"
    url = "page/05_faq/"
    weight = 2

[[menu.main]]
    parent = "docs"
    name = "Issues"
    url = "https://gitlab.com/graphviz/graphviz/issues"
    weight = 3



#[[menu.main]]
#    parent = "docs"
#    name = "Math Sample"
#    url = "post/2017-03-05-math-sample"
#    weight = 2

#[[menu.main]]
#    parent = "docs"
#    name = "Code Sample"
#    url = "post/2016-03-08-code-sample"
#    weight = 3


[[menu.main]]
    identifier = "about"
    name = "About"
    weight = 3


[[menu.main]]
    parent = "about"
    name = "GraphViz!"
    url = "page/new-about/"
    weight = 1

[[menu.main]]
    parent = "about"
    name = "Credits"
    url = "page/08_credits/"
    weight = 3

[[menu.main]]
    parent = "about"
    name = "Contact"
    url = "page/09_contact/"
    weight = 3


[[menu.main]]
    parent = "about"
    name = "License"
    url = "page/07_license/"
    weight = 3


#[[menu.main]]
#    identifier = "follow"
#    name = "Follow Us"
#    weight = 3

[[menu.main]]
    name = "Tweets"
    url = "tags"
    weight = 3

#[[menu.main]]
#    name = "Blog"
#    url = ""
#    weight = 1
